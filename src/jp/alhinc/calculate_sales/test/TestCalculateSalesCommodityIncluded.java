package jp.alhinc.calculate_sales.test;

import org.junit.Test;

public class TestCalculateSalesCommodityIncluded extends TestCalculateSalesBase {

	@Test
	public void コマンドライン引数なし() {
		String[] testDir = createTestPath(new String[] { "" }, 0, false);
		// deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		// assertOutputFile(testDir[0], false);
	}

	@Test
	public void コマンドライン引数複数() {
		String[] testDir = createTestPath(new String[] { "" }, 2, false);
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 引数の末尾がパスセパレータ() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "correct" }, 1, true);
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 正常系() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "correct" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 支店名に支店を含まない() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "correct", "branch", "name" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 支店別金額が最大値() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "correct", "sales", "amount", "branch" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 商品別金額が最大値() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "correct", "sales", "amount", "commodity" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 連番の最後がフォルダ() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "correct", "sales", "folder", "last" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 売上ファイルなし() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "correct", "sales", "number", "none" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 売上ファイル1つ() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "correct", "sales", "number", "one" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 売上ファイル複数() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "correct", "sales", "number", "toomany" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 支店コードにアルファベットを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "branch", "code", "include", "alphabet" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードにひらがなを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "branch", "code", "include", "hiragana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードに漢字を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "branch", "code", "include", "kanji" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードにカタカナを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "branch", "code", "include", "katakana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードに記号を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "branch", "code", "include", "symbol" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードが2桁() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "branch", "code", "length", "2" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードが4桁() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "branch", "code", "length", "4" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店定義ファイルが存在しない() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "branch", "exist" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルが存在しません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店定義の要素数が1() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "branch", "line", "1" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店定義の要素数が3() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "branch", "line", "3" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品コードにひらがなを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "commodity", "code", "include", "hiragana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品コードに漢字を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "commodity", "code", "include", "kanji" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品コードにカタカナを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "commodity", "code", "include", "katakana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品コードに記号を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "commodity", "code", "include", "symbol" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品コードが7桁() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "commodity", "code", "length", "7" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品コードが9桁() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "commodity", "code", "length", "9" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品定義ファイルが存在しない() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "commodity", "exist" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルが存在しません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品定義の要素数が1() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "commodity", "line", "1" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品定義の要素数が3() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "commodity", "line", "3" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "商品定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 金額上限を超えるため出力されない() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "outfile", "does-not-output" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "合計金額が10桁を超えました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 出力ファイルロック() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "outfile", "locked" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		// assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルの支店コードが存在しない() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "branchcode" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "00000001.rcdの支店コードが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルの商品コードが存在しない() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "commoditycode" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "00000005.rcdの商品コードが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上にアルファベットを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "include", "alphabet" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上にひらがなを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "include", "hiragana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上に漢字を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "include", "kanji" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上にカタカナを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "include", "katakana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上に記号を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "include", "symbol" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店別金額が上限超() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "over", "branch" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "合計金額が10桁を超えました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 商品別金額が上限超() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "amount", "over", "commodity" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "合計金額が10桁を超えました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルが2行() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "sales", "lines", "2" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "00000004.rcdのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルが4行() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "sales", "lines", "4" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "00000005.rcdのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルに異なる拡張子が存在() {
		String[] testDir = createTestPath(
				new String[] { "spec", "fixtures", "error", "sales", "sequence", "extension" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルにディレクトリを含む() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "sales", "sequence", "folder" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルが連番でない() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "sales", "sequence", "lost" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルの先頭にごみ() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "sales", "sequence", "prefix" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルの末尾にごみ() {
		String[] testDir = createTestPath(new String[] { "spec", "fixtures", "error", "sales", "sequence", "suffix" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	/**
	 * 前回出力ファイルを削除する（支店・商品）
	 *
	 * @param dirName
	 */
	private void deletePreviousFile(String dirName) {
		super.deletePreviousFile(dirName, outputBranchFileName);
		super.deletePreviousFile(dirName, outputCommodityFileName);
	}

	/**
	 * 出力ファイルの整合性テスト（支店・商品）
	 *
	 * @param dirName
	 * @param existFlg
	 */
	private void assertOutputFile(String dirName, boolean existFlg) {
		super.assertOutputFile(dirName, outputBranchFileName, answerBranchFileName, existFlg);
		super.assertOutputFile(dirName, outputCommodityFileName, answerCommodityFileName, existFlg);
	}
}
